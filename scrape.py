import requests
from bs4 import BeautifulSoup
import pandas as pd

res= requests.get('https://news.ycombinator.com/news')
soup= BeautifulSoup(res.text, 'html.parser')
links= soup.select('.storylink')
subtext= soup.select('.subtext')

def sorting(result):
    return sorted(result, key= lambda k:k['votes'],reverse=True)

def create_custom_news(links,subtext):
    result=[]
    for index, item in enumerate(links):       
        title= links[index].getText()
        link= links[index].get('href', None)
        vote= subtext[index].select('.score')
        if len(vote):
            points= int(vote[0].getText().replace(' points',''))
            result.append({'title':title,'link':link,'votes':points})
        
    return sorting(result)

output= create_custom_news(links,subtext)
headers= output[0].keys()
table= pd.DataFrame(output,columns=headers)
print(table)
